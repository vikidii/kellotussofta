#include "databasemanager.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlRecord>
#include <QDateTime>

DatabaseManager::DatabaseManager(const QString &path)
{
    // Path is not empty
    assert(path.size() != 0);

    m_db = QSqlDatabase::addDatabase("QODBC");

    QString dbName = QString("DRIVER={SQL Server Native Client 11.0};"
                             "SERVER=88.193.140.34;"
                             "DATABASE=%1;"
                             "UID='user';"
                             "PWD='pass';"
                             "WSID=.;").arg(path);

    m_db.setDatabaseName(dbName);

    if (!m_db.open())
    {
      qDebug() << "Error: connection with database failed!";
      qDebug() << "Error: " << m_db.lastError().text();
    }
    else
    {
      qDebug() << "Database: connection ok";

      createTablePlayers();
      createTableTimes();
    }
}

DatabaseManager::~DatabaseManager()
{
    if (m_db.isOpen()) {
        m_db.close();
    }
}

bool DatabaseManager::createTableTimes()
{
    bool success = true;

    QSqlQuery query;

    // Checks if the table 'times' already exists
    // and if it doesn't, creates it.
    bool suc = query.exec("IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'times')"
                            "CREATE TABLE times (id UNIQUEIDENTIFIER PRIMARY KEY DEFAULT newid(),"
                                                "name VARCHAR(50) REFERENCES players(name),"
                                                "time FLOAT,"
                                                "date DATETIME);");

    // DEBUGGING
    if (!suc)
    {
        qDebug() << "Couldn't create the table 'times': one might already exist.";
        qDebug() << "Error: " << m_db.lastError().text();

        success = false;
    }

    return success;
}

bool DatabaseManager::createTablePlayers()
{
    bool success = true;

    QSqlQuery query;

    // Checks if the table 'players' already exists
    // and if it doesn't, creates it.
    bool suc = query.exec("IF NOT EXISTS (SELECT * FROM sys.tables WHERE name = 'players')"
                            "CREATE TABLE players (name VARCHAR(50) PRIMARY KEY,"
                                                    "email VARCHAR(50) DEFAULT NULL, "
                                                    "telegram VARCHAR(50) DEFAULT NULL);");

    // DEBUGGING
    if (!suc)
    {
        qDebug() << "Couldn't create the table 'players': one might already exist.";
        qDebug() << "Error: " << m_db.lastError().text();

        success = false;
    }

    return success;
}

bool DatabaseManager::checkPlayerExist(const QString &name) const
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    QSqlQuery query;

    // Returns 1 if found, 0 if not found
    query.prepare("SELECT COUNT(*) FROM players WHERE EXISTS ( SELECT * FROM players WHERE name = (?) )");
    query.bindValue(0, name);

    query.exec();
    query.next();

    if(query.value(0).toInt() == 0) {
        return false;
    } else {
        return true;
    }
}

bool DatabaseManager::addTime(const QString& name, const QString &email, const QString &tg, const double& time)
{
    // Time is positive value
    assert(time > 0.0);

    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    // Email fits to database
    assert(email.size() <= 50);

    // Email is trimmed if not empty
    if(email.size() > 0) {
        assert(email.at(0) != " ");
        assert(*(email.end() - 1) != " ");
    }

    // Telegram nick fits to database
    assert(tg.size() <= 50);

    // Telegram is trimmed if not empty
    if(tg.size() > 0) {
        assert(tg.at(0) != " ");
        assert(*(tg.end() - 1) != " ");
    }

    bool success = true;

    // Add the player to table "players"
    addNewPlayer(name, email, tg);

    // DEBUGGING
    qDebug() << "Adding the time.";

    QSqlQuery queryAdd;

    queryAdd.prepare("INSERT INTO times (name, time, date) VALUES (:name, :time, :date);");
    queryAdd.bindValue(":name", name);
    queryAdd.bindValue(":time", time);

    // Get date and time
    QDateTime now = QDateTime::currentDateTime();

    queryAdd.bindValue(":date", now);

    if(!queryAdd.exec())
    {
        qDebug() << "add time failed: " << queryAdd.lastError();
        success = false;
    }

    return success;
}

bool DatabaseManager::addNewPlayer(const QString &name, const QString &email, const QString &tg)
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    // Email fits to database
    assert(email.size() <= 50);

    // Email is trimmed if not empty
    if(email.size() > 0) {
        assert(email.at(0) != " ");
        assert(*(email.end() - 1) != " ");
    }

    // Telegram nick fits to database
    assert(tg.size() <= 50);

    // Telegram is trimmed if not empty
    if(tg.size() > 0) {
        assert(tg.at(0) != " ");
        assert(*(tg.end() - 1) != " ");
    }

    bool success = true;

    qDebug() << "Creating new player " << name;

    QSqlQuery queryAdd;

    // Inserts the player if it doesn't already exist in 'players'.
    queryAdd.prepare("IF NOT EXISTS (SELECT * FROM players WHERE name = :name)"
                        "INSERT INTO players (name, email, telegram) VALUES (:name, :email, :tg);");
    queryAdd.bindValue(":name", name);
    queryAdd.bindValue(":email", email);
    queryAdd.bindValue(":tg", tg);

    if(!queryAdd.exec())
    {
        qDebug() << "add player failed: " << queryAdd.lastError();
        success = false;
    }

    return success;
}

bool DatabaseManager::removePlayer(const QString &name)
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    bool success = true;

    // If the times removal succeede, delete the player.
    if(removePlayerTimes(name)) {

        QSqlQuery removeQuery;
        removeQuery.prepare("DELETE FROM players WHERE name = :name;");
        removeQuery.bindValue(":name", name);

        if (!removeQuery.exec())
        {
            qDebug() << "remove player from players failed: " << removeQuery.lastError();
            success = false;

        }
    }
    return success;
}

bool DatabaseManager::removePlayerTimes(const QString &name)
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    bool success = true;

    QSqlQuery removeQuery;
    removeQuery.prepare("DELETE FROM times WHERE name = :name;");
    removeQuery.bindValue(":name", name);

    if (!removeQuery.exec())
    {
        qDebug() << "remove player times failed: " << removeQuery.lastError();
        success = false;

    }
    return success;
}

bool DatabaseManager::removeOneTime(const QString &name, const double &time)
{
    // Time is positive value
    assert(time > 0.0);

    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    bool success = true;

    QSqlQuery removeQuery;
    removeQuery.prepare("DELETE FROM times WHERE name = :name AND time = :time LIMIT 1;");
    removeQuery.bindValue(":name", name);
    removeQuery.bindValue(":time", time);

    if (!removeQuery.exec())
    {
        qDebug() << "remove player times failed: " << removeQuery.lastError();
        success = false;

    }
    return success;
}

bool DatabaseManager::removeLatestTime()
{
    bool success = true;

    // Deleting the last one
    QSqlQuery query;

    // Delete the latest time
    if(!query.exec("WITH MyCTE AS (SELECT TOP(1) * FROM times ORDER BY date DESC) DELETE FROM MyCTE;")) {
        qDebug() << "Removing latest failed: " << query.lastError();
        success = false;
    }

    return success;
}

bool DatabaseManager::removeAllTimes()
{
    bool success = true;

    QSqlQuery removeQuery;
    removeQuery.prepare("DELETE FROM times;");

    if (!removeQuery.exec())
    {
        qDebug() << "remove all times failed: " << removeQuery.lastError();
        return false;
    }

    return success;
}

bool DatabaseManager::removeAll()
{
    bool success = true;

    QSqlQuery removeQuery;
    removeQuery.prepare("DELETE FROM players;");

    if(!removeAllTimes() || !removeQuery.exec()) {
        qDebug() << "remove all failed: " << removeQuery.lastError();
        success = false;
    }

    return success;
}

std::vector<std::pair<QString, double>> DatabaseManager::getAllTimes() const
{
    std::vector<std::pair<QString, double>> times = {};

    QSqlQuery query("SELECT name, time FROM times ORDER BY date ASC;");

    while (query.next())
    {
        QString name = query.value(0).toString();
        double time = query.value(1).toDouble();

        times.emplace_back(std::make_pair(name, time));
    }

    return times;
}

std::unordered_map<std::string, double> DatabaseManager::getTimesOnDate(const QDate &date) const
{
    std::unordered_map<std::string, double> times = {};

    QSqlQuery query("SELECT * FROM times WHERE date = :date;");
    query.bindValue(":date", date);

    int iname = query.record().indexOf("name");
    int itime = query.record().indexOf("time");

    while (query.next())
    {
        QString name = query.value(iname).toString();
        std::string sname = name.toStdString();
        double time = query.value(itime).toDouble();

        times.insert({sname, time});
    }

    return times;
}

std::vector<std::pair<QString, double> > DatabaseManager::getTopTenTimes() const
{
    std::vector<std::pair<QString, double> > times = {};

    QSqlQuery query("SELECT TOP(10) name, MIN(time) as minTime FROM times GROUP BY name ORDER BY minTime ASC;");

    while (query.next())
    {
        QString name = query.value(0).toString();
        double time = query.value(1).toDouble();

        times.emplace_back(name, time);
    }

    // Return maximum of ten times
    assert(times.size() <= 10);

    return times;
}

std::vector<double> DatabaseManager::getPlayerTimes(const QString &name) const
{
    // Name is not empty
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    std::vector<double> times = {};

    QSqlQuery query("SELECT time FROM times WHERE name = :name;");
    query.bindValue(":name", name);

    while (query.next())
    {
        double time = query.value(0).toDouble();

        times.emplace_back(time);
    }

    return times;
}

std::vector<ClockedDrink> DatabaseManager::getPlayerClockedDrinks(const QString &name) const
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    std::vector<ClockedDrink> drinks = {};

    QSqlQuery query;
    query.prepare("SELECT name, time, date FROM times WHERE name = :name;");
    query.bindValue(":name", name);

    query.exec();

    while (query.next())
    {
        QString id = query.value(0).toString();
        double time = query.value(1).toDouble();
        QDateTime date = query.value(2).toDateTime();

        ClockedDrink drink = {id, time, date};

        drinks.emplace_back(drink);
    }

    return drinks;
}

double DatabaseManager::getAverageTime() const
{
    QSqlQuery query("SELECT AVG(time) FROM times;");

    query.first();

    return query.value(0).toDouble();
}

int DatabaseManager::getAmountOfClockings() const
{
    QSqlQuery query("SELECT COUNT(time) FROM times;");

    query.first();

    return query.value(0).toInt();
}

std::pair<QString, int> DatabaseManager::getMostClockedDrinks() const
{
    QSqlQuery query("SELECT name, NumOfClockings FROM "
                        "( SELECT name, COUNT(*) AS NumOfClockings FROM times GROUP BY name ) "
                            "AS clockings "
                    "ORDER BY NumOfClockings DESC, name ASC;");

    query.first();

    return std::make_pair(query.value(0).toString(), query.value(1).toInt());
}

int DatabaseManager::getAmountOfPlayers() const
{
    QSqlQuery query("SELECT COUNT(*) FROM players;");

    query.first();

    return query.value(0).toInt();
}

std::pair<QString, double> DatabaseManager::getBestAverageTime() const
{
    QSqlQuery query("SELECT TOP(1) name, AVG(time) AS AvgTime FROM times GROUP BY name ORDER BY AvgTime ASC;");

    query.first();

    return std::make_pair(query.value(0).toString(), query.value(1).toDouble());
}

Player DatabaseManager::getPlayerStats(const QString &name) const
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    // Player not found
    if(!checkPlayerExist(name)) {
        Player p = {"Player not found", "", "", {}};
        return p;
    }

    // Get the clocked drinks
    std::vector<ClockedDrink> drinks = getPlayerClockedDrinks(name);

    // Get the email and telegram nick
    std::pair<QString, QString> emailnTG = getPlayerEmailnTelegram(name);

    Player player = {name, emailnTG.first, emailnTG.second, drinks};
    return player;
}

std::map<QString, Player> DatabaseManager::getAllStats() const
{
    std::map<QString, Player> stats = {};

    std::vector<QString> names = getPlayerNames();

    for(auto name : names) {
        Player player = getPlayerStats(name);
        stats.insert({name, player});
    }

    return stats;
}

std::vector<QString> DatabaseManager::getPlayerNames() const
{
    std::vector<QString> names = {};

    QSqlQuery query("SELECT name FROM players;");

    while (query.next())
    {
        // Get name
        QString name = query.value(0).toString();

        names.emplace_back(name);
    }

    return names;
}

std::vector<QString> DatabaseManager::getAllEmails() const
{
    std::vector<QString> emails = {};

    QSqlQuery query("SELECT email FROM players;");

    while(query.next()) {
        emails.emplace_back(query.value(0).toString());
    }

    return emails;
}

std::vector<QString> DatabaseManager::getAllTelegrams() const
{
    std::vector<QString> telegrams = {};

    QSqlQuery query("SELECT telegram FROM players;");

    while(query.next()) {
        telegrams.emplace_back(query.value(0).toString());
    }

    return telegrams;
}

std::pair<QString, QString> DatabaseManager::getPlayerEmailnTelegram(const QString &name) const
{
    // Name is not empty string.
    assert(name.size() != 0);

    // Name is trimmed.
    assert(name.at(0) != " ");
    assert(*(name.end() - 1) != " ");

    QSqlQuery query;
    query.prepare("SELECT email, telegram FROM players WHERE name = (?);");
    query.bindValue(0, name);

    query.exec();
    query.next();

    return std::make_pair(query.value(0).toString(), query.value(1).toString());
}

bool DatabaseManager::changeName(const QString &oldName, const QString &newName)
{

}

bool DatabaseManager::changeEmail(const QString &name, const QString &newEmail)
{

}

bool DatabaseManager::changeTelegram(const QString &name, const QString &newTG)
{

}

/* DEBUGGING */
void DatabaseManager::printAllTimes() const
{
    qDebug() << "Persons in db:";

    QSqlQuery query("SELECT * FROM times;");

    int iID = query.record().indexOf("id");
    int iName = query.record().indexOf("name");
    int iTime = query.record().indexOf("time");
    int iDate = query.record().indexOf("date");
    int iClock = query.record().indexOf("clock");

    while (query.next())
    {
        int id = query.value(iID).toInt();
        QString name = query.value(iName).toString();
        double time = query.value(iTime).toDouble();
        QDate date = query.value(iDate).toDate();
        QTime clock = query.value(iClock).toTime();

        qDebug() << id << " ===" << name << ": " << time << ", " << date << " " << clock;
    }
}

void DatabaseManager::printAllPlayers() const
{
    qDebug() << "Players in db:";

    QSqlQuery query("SELECT * FROM players;");

    int name = query.record().indexOf("name");
    int email = query.record().indexOf("email");
    int tg = query.record().indexOf("telegram");

    while (query.next())
    {
        QString n = query.value(name).toString();
        QString e = query.value(email).toString();
        QString t = query.value(tg).toString();

        qDebug() << n << " ===" << e << ": " << t;
    }
}
