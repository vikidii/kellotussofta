#include "stopwatch.h"

void Stopwatch::start()
{
    running_ = true;
    starttime_ = Clock::now();
}

void Stopwatch::stop()
{
    running_ = false;
    elapsed_ += (Clock::now() - starttime_);
}

void Stopwatch::reset()
{
    running_ = false;
    elapsed_ = elapsed_.zero();
}

double Stopwatch::elapsed()
{
    if (!running_)
    {
        return static_cast<std::chrono::duration<double>>(elapsed_).count();
    }
    else
    {
        auto total = elapsed_ + (Clock::now() - starttime_);
        return static_cast<std::chrono::duration<double>>(total).count();
    }
}

bool Stopwatch::isRunnung() const
{
    return running_;
}
