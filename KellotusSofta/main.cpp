#include "windows/mainwindow.h"
#include <QApplication>

/*
 * TODO:
 * - All values from user in right format
 * - In submit, if player already exists, but has no tg/email
 *   but those are now filled with submitting time,
 *   updates these to player.
 * - Pre and Post conditions
 * - Add value and error checkings to everything
 * - Add Unit Tests
 * - Luokkien alustuslistat kuntoon
 * - If window already open, clicking the open icon
 *   in mainWindow shoud pop it up
 * - Stat window to show stats for different things chosen by user
 *   - Size based (top)stats
 *   - Event --
 * - Create login window
 * - Different auchtorities to users
 * - Window where you can update/change player information
 *
 * - Check player exist where needed: at least deleteWindow search
 *
 * - Login for dbm
 *   - set database to use
 *
 * - DBM: Take the "success" phrases out, make more simple
 * - DBM: Add checkings for inputs
 * - DBM: Add all other error checkings
 * - DBM: Make all std::strings to be QStrings
 * - DBM: Size value for drink
 * - DBM: Event value for drink
 * - DBM: ON DELETE ON UPDATE
 *
 */

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
