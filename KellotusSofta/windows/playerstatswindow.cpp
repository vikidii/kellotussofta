#include "playerstatswindow.h"
#include "ui_playerstatswindow.h"

#include <limits>

#include <QDebug> // Debugging

PlayerStatsWindow::PlayerStatsWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PlayerStatsWindow)
{
    ui->setupUi(this);

    // Connect search button
    connect(ui->pushButton, &QPushButton::clicked, this, &PlayerStatsWindow::updateData);
}

PlayerStatsWindow::~PlayerStatsWindow()
{
    delete ui;
}

void PlayerStatsWindow::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
}

void PlayerStatsWindow::updateData()
{
    // Get the name wanted
    QString name = ui->nameCombo->currentText();

    // Get player stats
    Player playerStats = m_dbm->getPlayerStats(name);

    // Clear the list of clocked drinks
    ui->listWidget->clear();

    double bestTime = INT_MAX;
    double averageTime = 0;

    // Add the drinks to the list
    int n = 1;
    for(auto drink = playerStats.drinks.begin(); drink != playerStats.drinks.end(); ++drink) {
        // New item for list
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);

        // The text
        QString text = QString(QString::number(n) + ".  " + QString::number(drink->time, 'f', 2) + "\n"
                               + drink->date.toString());

        item->setText(text);

        // Add the item to the list
        ui->listWidget->addItem(item);

        // Find the best time
        if(drink->time < bestTime) {
            bestTime = drink->time;
        }

        // Calculate the average time
        averageTime += drink->time;

        n++;
    }

    // Calculate the final average of times
    averageTime = averageTime/playerStats.drinks.size();

    // Set name
    ui->nameLabel->setText(playerStats.name);

    // Set email
    if(playerStats.email != "") {
        ui->emailLabel->setText(playerStats.email);
    } else {
        ui->emailLabel->setText("-");
    }

    // Set telegram
    if(playerStats.telegram != "") {
        ui->telegramLabel->setText(playerStats.telegram);
    } else {
        ui->telegramLabel->setText("-");
    }

    // Set amount of clocked drinks
    ui->amountLabel->setText(QString::number(playerStats.drinks.size()));

    // Best time and average time
    if(playerStats.drinks.size() != 0) {
        ui->bestLabel->setText(QString::number(bestTime, 'f', 2));
        ui->avgLabel->setText(QString::number(averageTime, 'f', 2));
    } else {
        ui->bestLabel->setText("-");
        ui->avgLabel->setText("-");
    }
}

void PlayerStatsWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);

    // Clear the list
    ui->listWidget->clear();

    // Clear name label
    ui->nameLabel->clear();

    // Clear email label
    ui->emailLabel->clear();

    // Clear telegram label
    ui->telegramLabel->clear();

    // Clear amount of clocked drinks label
    ui->amountLabel->clear();

    // Clear best time label
    ui->bestLabel->clear();

    // Clear average time label
    ui->avgLabel->clear();
}

void PlayerStatsWindow::fillComboBox()
{
    // Get all names
    std::vector<QString> names = m_dbm->getPlayerNames();

    // Clear names from combo box
    ui->nameCombo->clear();

    // Add names to combo box
    for(auto name = names.begin(); name != names.end(); ++name) {
        ui->nameCombo->addItem(*name);
    }

    // Set to be empty
    ui->nameCombo->setCurrentText("");
}
