#ifndef STATWINDOW_H
#define STATWINDOW_H

#include <QMainWindow>

#include "databasemanager.h"

namespace Ui {
class StatWindow;
}

class StatWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit StatWindow(QWidget *parent = nullptr);
    ~StatWindow();

    void addDBM(DatabaseManager *dbm);

public slots:
    void updateData();

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    Ui::StatWindow *ui;

    DatabaseManager *m_dbm;
};

#endif // STATWINDOW_H
