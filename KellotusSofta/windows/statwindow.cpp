#include "statwindow.h"
#include "ui_statwindow.h"

#include <QPixmap>
#include <QKeyEvent>

#include <QDebug>

StatWindow::StatWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StatWindow()),
    m_dbm(nullptr)
{
    ui->setupUi(this);

    QPixmap logo = QPixmap(":/logos/resources/NMKSV -haalarimerkki_ei_taustaa.png");
    ui->label->setPixmap(logo.scaled(200,200,Qt::KeepAspectRatio));

    // Connect update button to update function
    connect(ui->updateButton, &QPushButton::clicked, this, &StatWindow::updateData);
}

StatWindow::~StatWindow()
{
    delete ui;
}

void StatWindow::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
    ui->osWidget->addDBM(dbm);
    ui->TTWidget->addDBM(dbm);
}

void StatWindow::updateData()
{
    ui->osWidget->updateData();
    ui->TTWidget->updateData();
}

void StatWindow::keyPressEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Return)
    {
        qDebug() << "Pressed enter in stats.";
    }
}
