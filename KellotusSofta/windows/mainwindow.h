#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QTimer>
#include <memory>

#include "../databasemanager.h"
#include "../stopwatch.h"

#include "statwindow.h"
#include "deletewindow.h"
#include "alltimeswindow.h"
#include "playerstatswindow.h"
#include "loginwindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void startClock();
    void roundClock();
    void stopClock();
    void resetClock();

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void closeEvent(QCloseEvent *event);

private:
    void updateTime();

    // Times clocked
    std::vector<double> m_clockedTimes;

    Ui::MainWindow *ui;

    // Needed help elements
    DatabaseManager *m_dbm; // DataBase Manager
    Stopwatch *m_stopwatch; // Stopwatch
    QTimer *m_timer; // Interval Timer

    // All windows
    StatWindow *m_statWindow;
    DeleteWindow *m_deleteWindow;
    AllTimesWindow *m_allTimesWindow;
    PlayerStatsWindow *m_playerStatsWindow;
    LoginWindow *m_loginWindow;
};

#endif // MAINWINDOW_H
