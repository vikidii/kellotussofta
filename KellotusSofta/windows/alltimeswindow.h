#ifndef ALLTIMESWINDOW_H
#define ALLTIMESWINDOW_H

#include <QMainWindow>

#include "databasemanager.h"

namespace Ui {
class AllTimesWindow;
}

class AllTimesWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AllTimesWindow(QWidget *parent = nullptr);
    ~AllTimesWindow();

    void addDBM(DatabaseManager *dbm);

    void updateData();

private:
    Ui::AllTimesWindow *ui;

    DatabaseManager *m_dbm;
};

#endif // ALLTIMESWINDOW_H
