#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>

#include "../databasemanager.h"

namespace Ui {
class LoginWindow;
}

class LoginWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit LoginWindow(QWidget *parent = nullptr);
    ~LoginWindow();

    void addDBM(DatabaseManager *dbm);

private:
    Ui::LoginWindow *ui;

    DatabaseManager *m_dbm;
};

#endif // LOGINWINDOW_H
