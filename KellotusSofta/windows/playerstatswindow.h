#ifndef PLAYERSTATSWINDOW_H
#define PLAYERSTATSWINDOW_H

#include <QMainWindow>

#include "databasemanager.h"

namespace Ui {
class PlayerStatsWindow;
}

class PlayerStatsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PlayerStatsWindow(QWidget *parent = nullptr);
    ~PlayerStatsWindow();

    void addDBM(DatabaseManager *dbm);

    void fillComboBox();

public slots:
    void updateData();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::PlayerStatsWindow *ui;

    DatabaseManager *m_dbm;
};

#endif // PLAYERSTATSWINDOW_H
