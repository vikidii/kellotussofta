#include "deletewindow.h"
#include "ui_deletewindow.h"

#include <QDebug> // DEBUGGING

DeleteWindow::DeleteWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DeleteWindow)
{
    ui->setupUi(this);

    // Connect search button to list widget updating
    connect(ui->searchButton, &QPushButton::clicked, this, &DeleteWindow::updateListData);

    // Connect delete buttons
    connect(ui->dLatestButton, &QPushButton::clicked, this, &DeleteWindow::deleteLatest);
    connect(ui->dAllButton, &QPushButton::clicked, this, &DeleteWindow::deleteAllPlayers);
    connect(ui->dPlayerButton, &QPushButton::clicked, this, &DeleteWindow::deletePlayerTimes);
    connect(ui->dAllTimesButton, &QPushButton::clicked, this, &DeleteWindow::deleteAllTimes);
    connect(ui->dChosenTimeButton, &QPushButton::clicked, this, &DeleteWindow::deleteChosenTime);
}

DeleteWindow::~DeleteWindow()
{
    delete ui;
}

void DeleteWindow::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
}

void DeleteWindow::updateComboBox()
{
    // Get all names
    std::vector<QString> names = m_dbm->getPlayerNames();

    // Clear names from combo box
    ui->comboBox->clear();

    // Add names to combo box
    for(auto name = names.begin(); name != names.end(); ++name) {
        ui->comboBox->addItem(*name);
    }

    // Set to be empty
    ui->comboBox->setCurrentText("");
}

void DeleteWindow::updateListData()
{
    // Get the name wanted
    QString name = ui->comboBox->currentText();

    // Get player stats
    Player playerStats = m_dbm->getPlayerStats(name);

    // Clear the list of clocked drinks
    ui->listWidget->clear();

    // Add the drinks to the list
    int n = 1;
    for(auto drink = playerStats.drinks.begin(); drink != playerStats.drinks.end(); ++drink) {
        // New item for list
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);

        // The text
        QString text = QString(QString::number(n) + ".  " + QString::number(drink->time, 'f', 2) + "\n"
                               + drink->date.toString());

        // Add the text to item
        item->setText(text);

        // Add the item to the list
        ui->listWidget->addItem(item);

        n++;
    }
}

void DeleteWindow::deleteLatest()
{
    // Delete the latest submitted time
    m_dbm->removeLatestTime();
}

void DeleteWindow::deleteChosenTime()
{
    // Get the pressed time from list

    // Delete it
}

void DeleteWindow::deletePlayerTimes()
{
    QString name = ui->comboBox->currentText();
    m_dbm->removePlayerTimes(name);
}

void DeleteWindow::deleteAllTimes()
{
    // Delete all times
    m_dbm->removeAllTimes();
}

void DeleteWindow::deleteAllPlayers()
{
    // Delete all players
    // Deletes also all times
    m_dbm->removeAll();
}

void DeleteWindow::closeEvent(QCloseEvent *event)
{
    // Event data not used
    Q_UNUSED(event);

    // Clear the list
    ui->listWidget->clear();
}
