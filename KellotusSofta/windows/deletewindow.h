#ifndef DELETEWINDOW_H
#define DELETEWINDOW_H

#include <QMainWindow>

#include "databasemanager.h"

namespace Ui {
class DeleteWindow;
}

class DeleteWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DeleteWindow(QWidget *parent = nullptr);
    ~DeleteWindow();

    void addDBM(DatabaseManager *dbm);

    void updateComboBox();

public slots:
    void updateListData();

    void deleteLatest();
    void deleteChosenTime();
    void deletePlayerTimes();
    void deleteAllTimes();
    void deleteAllPlayers();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::DeleteWindow *ui;

    DatabaseManager *m_dbm;
};

#endif // DELETEWINDOW_H
