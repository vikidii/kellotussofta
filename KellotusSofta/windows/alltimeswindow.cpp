#include "alltimeswindow.h"
#include "ui_alltimeswindow.h"

AllTimesWindow::AllTimesWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AllTimesWindow)
{
    ui->setupUi(this);
}

AllTimesWindow::~AllTimesWindow()
{
    delete ui;
}

void AllTimesWindow::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
    ui->timesWidget->addBDM(dbm);
}

void AllTimesWindow::updateData()
{
    ui->timesWidget->updateData();
}
