#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <windows.h>
#include <QGridLayout>
#include <QKeyEvent>

#include <QDebug> // DEBUG

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_clockedTimes({}),
    ui(new Ui::MainWindow),
    m_dbm(nullptr),
    m_stopwatch(new Stopwatch()),
    m_timer(new QTimer()),
    m_statWindow(new StatWindow()),
    m_deleteWindow(new DeleteWindow()),
    m_allTimesWindow(new AllTimesWindow()),
    m_playerStatsWindow(new PlayerStatsWindow()),
    m_loginWindow(new LoginWindow())
{
    // Init database manager
    // Needs to be initialized before ui,
    // 'cause ui uses this in constructor
    m_dbm = new DatabaseManager("KELLOTUS_TEST");

    ui->setupUi(this);

    // Give dbm to those who need it
    ui->submitWidget->addDBM(m_dbm);
    m_statWindow->addDBM(m_dbm);
    m_allTimesWindow->addDBM(m_dbm);
    m_playerStatsWindow->addDBM(m_dbm);
    m_deleteWindow->addDBM(m_dbm);
    m_loginWindow->addDBM(m_dbm);

    /* TOOLBAR */

    // Stat Window
    QPixmap statsPix = QPixmap(":/logos/resources/stats_win_logo.png");
    QAction *stats = ui->toolBar->addAction(QIcon(statsPix), "Overall Statistics");
    connect(stats, &QAction::triggered, this, [=](){ m_statWindow->show();
                                                    m_statWindow->updateData(); });

    // Player stats window
    QPixmap playerStatsPix = QPixmap(":/logos/resources/people_win_logo.png");
    QAction *playerStats = ui->toolBar->addAction(QIcon(playerStatsPix), "Player Statistics");
    connect(playerStats, &QAction::triggered, this, [=](){ m_playerStatsWindow->show();
                                                        m_playerStatsWindow->fillComboBox(); });

    // All times window
    QPixmap allTimesPix = QPixmap(":/logos/resources/all_times_win_logo.png");
    QAction *allTimes = ui->toolBar->addAction(QIcon(allTimesPix), "All Clockings");
    connect(allTimes, &QAction::triggered, this, [=](){ m_allTimesWindow->show();
                                                        m_allTimesWindow->updateData(); });

    // Separator
    ui->toolBar->addSeparator();

    // Delete window
    QPixmap deletePix = QPixmap(":/logos/resources/delete_win_logo.png");
    QAction *deleteTime = ui->toolBar->addAction(QIcon(deletePix), "Delete Times/Players");
    connect(deleteTime, &QAction::triggered, this, [=](){ m_deleteWindow->show();
                                                        m_deleteWindow->updateComboBox(); });

    /* !TOOLBAR */

    /* CONNECTIONS */

    // Menubar to windows
    connect(ui->actionstatistics, &QAction::triggered, this, [=](){ m_statWindow->show();
                                                                    m_statWindow->updateData(); });

    connect(ui->actionDeleting, &QAction::triggered, this, [=](){ m_deleteWindow->show();
                                                                    m_deleteWindow->updateComboBox(); });

    connect(ui->actionPlayer_Stats, &QAction::triggered, this, [=](){ m_playerStatsWindow->show();
                                                                        m_playerStatsWindow->fillComboBox(); });

    connect(ui->actionAll_Times, &QAction::triggered, this, [=](){ m_allTimesWindow->show();
                                                                    m_allTimesWindow->updateData(); });

    connect(ui->actionLogin_Logout, &QAction::triggered, this, [=](){ m_loginWindow->show(); });

    // Connect timer to update time
    connect(m_timer, &QTimer::timeout, this, &MainWindow::updateTime);

    // Timer buttons
    connect(ui->startButton, &QPushButton::clicked, this, &MainWindow::startClock);
    connect(ui->roundButton, &QPushButton::clicked, this, &MainWindow::roundClock);
    connect(ui->stopButton, &QPushButton::clicked, this, &MainWindow::stopClock);
    connect(ui->resetButton, &QPushButton::clicked, this, &MainWindow::resetClock);

    /* !CONNECTIONS */

    // Status bar
    statusBar()->showMessage("Ready");
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    // Could do with switch case

    if(event->key() == Qt::Key_Return)
    {
        if(m_stopwatch->isRunnung()) {
            // STOP STOPWATCH
            stopClock();
        } else {
            // START STOPWATCH
            startClock();
        }
    }

    else if(event->key() == Qt::Key_Shift)
    {
        // ROUND STOPWATCH
        if(m_stopwatch->isRunnung()) {
            roundClock();
        }
    }

}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        qDebug() << "Clicked left";
    }

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    // Closes all the other windows when main window is closed
    foreach(QWidget *widget, QApplication::topLevelWidgets()) {
        // Main window closed last
        if (widget == this)
            continue;
        widget->close();
    }
    // Accept main window closing
    event->accept();
}

void MainWindow::updateTime()
{
    ui->timeLabel->setText(QString::number(m_stopwatch->elapsed(), 'f', 2));
}

MainWindow::~MainWindow()
{
    delete ui;
    delete m_dbm;
    delete m_stopwatch;
    delete m_timer;

    // Delete windows
    delete m_allTimesWindow;
    delete m_deleteWindow;
    delete m_playerStatsWindow;
    delete m_statWindow;
    delete m_loginWindow;
}

void MainWindow::startClock()
{
    if(m_stopwatch->elapsed() == 0.0) {
        m_stopwatch->start();

        // Update timer display label every 10ms
        m_timer->start(10);

        m_clockedTimes.clear();
        ui->submitWidget->clearComboBox();
    } else {
        ui->infoLabel->setText("Reset the stopwatch!");
    }
}

void MainWindow::roundClock()
{
    if(m_stopwatch->isRunnung()) {
        double time = m_stopwatch->elapsed();
        m_clockedTimes.emplace_back(time);

        // Show the round time for 0.5 seconds
        ui->roundTimeLabel->setText(QString::number(time, 'f', 2));
        QTimer::singleShot(500, this, [=](){ ui->roundTimeLabel->setText(""); });
    }
}

void MainWindow::stopClock()
{
    if(m_stopwatch->isRunnung()) {
        m_stopwatch->stop();

        // No need to update the display every 10ms
        m_timer->stop();

        m_clockedTimes.emplace_back(m_stopwatch->elapsed());
        ui->infoLabel->setText("");
        ui->submitWidget->fillComboBox(m_clockedTimes);
    }
}

void MainWindow::resetClock()
{
    if(!m_stopwatch->isRunnung()) {
        m_stopwatch->reset();
        ui->infoLabel->setText("");
    } else {
        ui->infoLabel->setText("Stop the clock first!");
    }

    // Update stats
    if(!m_statWindow->isHidden())
        qDebug() << "Update statWindow from mainwindow";

        m_statWindow->updateData();
}
