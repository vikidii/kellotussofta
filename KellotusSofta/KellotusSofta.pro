#-------------------------------------------------
#
# Project created by QtCreator 2019-03-29T15:21:25
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KellotusSofta
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
    windows/mainwindow.cpp \
    databasemanager.cpp \
    stopwatch.cpp \
    windows/statwindow.cpp \
    widgets/oswidget.cpp \
    widgets/toptenwidget.cpp \
    widgets/clockedtimewidget.cpp \
    widgets/timeswidget.cpp \
    windows/deletewindow.cpp \
    windows/alltimeswindow.cpp \
    windows/playerstatswindow.cpp \
    widgets/submittimewidget.cpp \
    windows/loginwindow.cpp

HEADERS += \
    windows/mainwindow.h \
    databasemanager.h \
    stopwatch.h \
    windows/statwindow.h \
    widgets/oswidget.h \
    widgets/toptenwidget.h \
    widgets/clockedtimewidget.h \
    widgets/timeswidget.h \
    windows/deletewindow.h \
    windows/alltimeswindow.h \
    windows/playerstatswindow.h \
    widgets/submittimewidget.h \
    windows/loginwindow.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

FORMS += \
    mainwindow.ui \
    statwindow.ui \
    oswidget.ui \
    toptenwidget.ui \
    clockedtimewidget.ui \
    timeswidget.ui \
    deletewindow.ui \
    alltimeswindow.ui \
    playerstatswindow.ui \
    submittimewidget.ui \
    loginwindow.ui
