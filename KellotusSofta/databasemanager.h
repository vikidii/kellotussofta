#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <unordered_map>
#include <map>
#include <vector>
#include <QDate>
#include <QTime>


// Describes one clocked drink
struct ClockedDrink {
    QString orderID; // Drinks order nro in sql database
    double time; // Clocked time
    QDateTime date; // Time when clocked
};

// Describes one player
struct Player {
    QString name;
    QString email;
    QString telegram;
    std::vector<ClockedDrink> drinks; // Clocked drinks
};

class DatabaseManager
{
public:
    /**
     * @brief DatabaseManager
     * Constructor, opens connection and tries to create tables
     *
     * @param path
     * Name for the SQL server database.
     *
     * @pre path is not empty string.
     */
    DatabaseManager(const QString& path);
    ~DatabaseManager();

    /* ADDING */

    /**
     * @brief addTime
     * Adds the player to the database
     * and adds the time to the table "times"
     *
     * @param name
     * Name of the player.
     *
     * @param email
     * Players email.
     *
     * @param tg
     * Players telegram nick
     *
     * @param time
     * Time of the clocking.
     *
     * @return
     * Success of the insertion.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     * @pre email is trimmed.
     * @pre tg is trimmed.
     * @pre email is not over 50 chars.
     * @pre tg is not over 50 chars.
     * @pre time is positive value.
     */
    bool addTime(const QString& name, const QString& email, const QString &tg, const double &time);

    /**
     * @brief addNewPlayer
     * Adds a new player to to table "players"
     * if one does not already exist.
     *
     * @param name
     * Name of the player, not NULL.
     *
     * @param email
     * Email of the player.
     *
     * @param tg
     * Telegram tag for the user.
     *
     * @return
     * Success of the insertion.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     * @pre email is trimmed.
     * @pre tg is trimmed.
     * @pre email is not over 50 chars.
     * @pre tg is not over 50 chars.
     */
    bool addNewPlayer(const QString& name, const QString& email, const QString &tg);

    /* REMOVING */

    /**
     * @brief removePlayer
     * Removes the player from table "players"
     * and all of his/her times from database.
     *
     * @param name
     * Name of the player to be deleted.
     *
     * @return
     * Success of the deletion.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     */
    bool removePlayer(const QString& name);

    /**
     * @brief removePlayerTimes
     * Removes all the player's times from the table "times"
     *
     * @param name
     * Name of the player.
     *
     * @return
     * Success of the removal.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     */
    bool removePlayerTimes(const QString& name);

    /*
     *
     *
     * pre:
     * post:
     */
    bool removeOneTime(const QString& name, const double &time);

    /**
     * @brief removeLatestTime
     * Removes the latest clocked time from table "times"
     *
     * @return
     * Success of the removal.
     */
    bool removeLatestTime();

    /* HARD REMOVING */

    /**
     * @brief removeAllTimes
     * Removes all data from table "times".
     *
     * @return
     * Success of the removal.
     */
    bool removeAllTimes();

    /**
     * @brief removeAll
     * Deletes all data from tables "times" and "players".
     *
     * @return
     * Success of the removal.
     */
    bool removeAll();

    /* GET TIMES */

    /**
     * @brief getAllTimes
     * Gets all of the times in table "times".
     *
     * @return
     * All of the times, {name, time}.
     */
    std::vector<std::pair<QString, double>> getAllTimes() const;

    /*
     *
     *
     * pre:
     * post:
     */
    std::unordered_map<std::string, double> getTimesOnDate(const QDate& date) const;

    /**
     * @brief getTopTenTimes
     * Gets the 10 best times from different persons
     * from table "times". If there is less times or less players
     * than 10, function returns as many times as there is to return.
     *
     * @return
     * Ten best times in ascending order by time, {name, time}.
     *
     * @post return vector::size() <= 10.
     */
    std::vector<std::pair<QString, double>> getTopTenTimes() const;

    /**
     * @brief getPlayerTimes
     * Returns all times for the given name from
     * table "times".
     *
     * @param name
     * Name of the player.
     *
     * @return
     * All times for the given name.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     */
    std::vector<double> getPlayerTimes(const QString& name) const;

    /**
     * @brief getPlayerClockedDrinks
     * Gets all of the drinks a player has clocked.
     *
     * @param name
     * Name of the player.
     *
     * @return
     * All of the drinks.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     *
     * @todo datetime is smarter format.
     */
    std::vector<ClockedDrink> getPlayerClockedDrinks(const QString& name) const;

    /* FOR OVERALL STATS */

    /**
     * @brief getAverageTime
     * Gets the average time of clockings from table "times".
     *
     * @return
     * The average time.
     */
    double getAverageTime() const;

    /**
     * @brief getAmountOfClockings
     * Gets the amount of clockings in table "times".
     *
     * @return
     * The amount of times.
     */
    int getAmountOfClockings() const;

    /**
     * @brief getMostClockedDrinks
     * Gets the player who has most clocked drinks.
     * In case of same amount on multiple people,
     * alphabetically first gets chosen.
     *
     * @return
     * The amount of clocked drinks, {player name, amount}.
     *
     * @todo something smarter than alphabetical by name.
     */
    std::pair<QString, int> getMostClockedDrinks() const;

    /**
     * @brief getAmountOfPlayers
     * Gets the amount of players in table "players".
     *
     * @return
     * The amount of players.
     *
     * @todo Doesn't take in count if multiple have same ave time.
     */
    int getAmountOfPlayers() const;

    /**
     * @brief getBestAverageTime
     * Gets the player that has the best average time.
     *
     * @return
     * Player and the average time, {player name, average time}.
     *
     * @todo Doesn't take in count if multiple have same amount.
     */
    std::pair<QString, double> getBestAverageTime() const;

    /* GET STATS */

    /**
     * @brief getPlayerStats
     * Gets all available data for player.
     *
     * @param name
     * Name of the player.
     *
     * @return
     * The data of the player.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     */
    Player getPlayerStats(const QString& name) const;

    /**
     * @brief getAllStats
     * Gets all players and their stats.
     *
     * @return
     * Players and their stats, {palyer name, player stats}.
     */
    std::map<QString, Player> getAllStats() const;

    /* GET PLAYER INFOS */

    /**
     * @brief getPlayerNames
     * Gets all the player names.
     *
     * @return
     * All names.
     */
    std::vector<QString> getPlayerNames() const;

    /**
     * @brief getAllEmails
     * Gets all emails of players.
     *
     * @return
     * All emails.
     */
    std::vector<QString> getAllEmails() const;

    /**
     * @brief getAllTelegrams
     * Gets all telegram nicks of players.
     *
     * @return
     * All telegram nicks.
     */
    std::vector<QString> getAllTelegrams() const;

    /**
     * @brief getPlayerEmailnTelegram
     * Gets players email and telegram nick.
     *
     * @param name
     * Name of the player.
     *
     * @return
     * {players email, players telegram nick}.
     *
     * @pre name is not empty string.
     * @pre name is trimmed.
     */
    std::pair<QString, QString> getPlayerEmailnTelegram(const QString& name) const;

    /* EDIT PLAYER DATA */

    bool changeName(const QString &oldName, const QString &newName); // TODO
    bool changeEmail(const QString &name, const QString &newEmail); // TODO
    bool changeTelegram(const QString &name, const QString &newTG); // TODO

    /* DEBUG */
    void printAllTimes() const;
    void printAllPlayers() const;

private:
    /* CRATE TABLES */

    /**
     * @brief createTableTimes
     * Creates the table "times".
     *
     * @return
     * Succession of the table creation.
     */
    bool createTableTimes();

    /**
     * @brief createTablePlayers
     * Creates the table "players".
     *
     * @return
     * Succession of the table creation.
     */
    bool createTablePlayers();

    bool checkPlayerExist(const QString &name) const;

    /* THE DATABASE */

    QSqlDatabase m_db;
};

#endif // DATABASEMANAGER_H
