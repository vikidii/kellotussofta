#ifndef CLOCKEDTIMEWIDGET_H
#define CLOCKEDTIMEWIDGET_H

#include <QWidget>

namespace Ui {
class ClockedTimeWidget;
}

class ClockedTimeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ClockedTimeWidget(QWidget *parent = nullptr);
    ~ClockedTimeWidget();

    void addStats(std::string nro, std::string name, double time);

private:
    Ui::ClockedTimeWidget *ui;
};

#endif // CLOCKEDTIMEWIDGET_H
