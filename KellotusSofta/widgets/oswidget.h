#ifndef OSWIDGET_H
#define OSWIDGET_H

// Overall Statistics Widget

#include <QWidget>

#include "databasemanager.h"

namespace Ui {
class OSWidget;
}

class OSWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OSWidget(QWidget *parent = nullptr);
    ~OSWidget();

    void addDBM(DatabaseManager *dbm);
    void updateData();

private:
    Ui::OSWidget *ui;

    DatabaseManager *m_dbm;
};

#endif // OSWIDGET_H
