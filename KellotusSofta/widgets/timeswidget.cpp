#include "../widgets/timeswidget.h"
#include "ui_timeswidget.h"

#include <QDebug> // Debugging

TimesWidget::TimesWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimesWidget)
{
    ui->setupUi(this);

    // Connect the update button to update function
    connect(ui->updateButton, &QPushButton::clicked, this, &TimesWidget::updateData);
}

TimesWidget::~TimesWidget()
{
    delete ui;
}

void TimesWidget::addBDM(DatabaseManager *dbm)
{
    m_dbm = dbm;
}

void TimesWidget::updateData()
{
    /* TEXT NEEDS FORMATTING */

     std::vector<std::pair<QString, double>> times = m_dbm->getAllTimes();

     // Clear the previous items
     ui->listWidget->clear();

     // Add the item to the list
     int n = 1; // Iterator
     for(auto time = times.begin(); time != times.end(); ++time) {

        // New item for list
        QListWidgetItem *item = new QListWidgetItem(ui->listWidget);

        // The text
        QString text = QString(QString::number(n) + ". " + time->first + ":   " + QString::number(time->second, 'f', 2));

        // Add the text to item
        item->setText(text);

        // Add the item to the list
        ui->listWidget->addItem(item);
        n++;
     }
}
