#ifndef TOPTENWIDGET_H
#define TOPTENWIDGET_H

#include <QWidget>

#include "databasemanager.h"

#include "../widgets/clockedtimewidget.h"

namespace Ui {
class TopTenWidget;
}

class TopTenWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TopTenWidget(QWidget *parent = nullptr);
    ~TopTenWidget();

    void addDBM(DatabaseManager *dbm);
    void updateData();

private:
    void clearTimes();

    Ui::TopTenWidget *ui;

    DatabaseManager *m_dbm;

    std::vector<ClockedTimeWidget*> m_times;
};

#endif // TOPTENWIDGET_H
