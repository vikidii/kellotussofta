#include "../widgets/oswidget.h"
#include "ui_oswidget.h"

// Overall Statistics Widget

OSWidget::OSWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OSWidget),
    m_dbm(nullptr)
{
    ui->setupUi(this);
}

OSWidget::~OSWidget()
{
    delete ui;
}

void OSWidget::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
}

void OSWidget::updateData()
{
    // Amount of clockings
    int amountOfClockings = m_dbm->getAmountOfClockings();
    ui->totalAmountLabel->setText(QString::number(amountOfClockings));

    // Set average time
    ui->averageTimeLabel->setText(QString::number(m_dbm->getAverageTime(), 'f', 2));

    // Most clocked drinks on person
    std::pair<QString, int> clockedMost = m_dbm->getMostClockedDrinks();
    ui->mostPersonLabel->setText(clockedMost.first + ": " + QString::number(clockedMost.second));

    // Average amount of clocked drinks
    int amountOfPlayers = m_dbm->getAmountOfPlayers();
    ui->averageAmountLabel->setText(QString::number(double(amountOfClockings)/double(amountOfPlayers), 'f', 2));

    // Best average time on person
    std::pair<QString, double> bestAve = m_dbm->getBestAverageTime();
    ui->bestAveTimePLabel->setText(bestAve.first + ": " + QString::number(bestAve.second, 'f', 2));

    // Amount of players
    ui->playerAmountLabel->setText(QString::number(amountOfPlayers));
}
