#ifndef SUBMITTIMEWIDGET_H
#define SUBMITTIMEWIDGET_H

#include <QWidget>

#include <databasemanager.h>

namespace Ui {
class SubmitTimeWidget;
}

class SubmitTimeWidget : public QWidget
{
    Q_OBJECT

public:
    explicit SubmitTimeWidget(QWidget *parent = nullptr);
    ~SubmitTimeWidget();

    void addDBM(DatabaseManager *dbm);

    void fillComboBox(const std::vector<double> &times);
    void clearComboBox();

private:
    void submitTime();
    void updateNameCombo();

    Ui::SubmitTimeWidget *ui;

    DatabaseManager *m_dbm;
};

#endif // SUBMITTIMEWIDGET_H
