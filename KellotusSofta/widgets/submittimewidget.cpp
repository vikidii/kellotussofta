#include "submittimewidget.h"
#include "ui_submittimewidget.h"

#include <QTimer>

#include <QDebug> // Debugging

SubmitTimeWidget::SubmitTimeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SubmitTimeWidget),
    m_dbm(nullptr)
{
    ui->setupUi(this);

    connect(ui->submitButton, &QPushButton::clicked, this, &SubmitTimeWidget::submitTime);

    updateNameCombo();
}

SubmitTimeWidget::~SubmitTimeWidget()
{
    delete ui;
}

void SubmitTimeWidget::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
}

void SubmitTimeWidget::fillComboBox(const std::vector<double> &times)
{
    for(auto time = times.begin(); time != times.end(); ++time) {
        ui->comboBox->addItem(QString::number(*time, 'f', 3));
    }
}

void SubmitTimeWidget::clearComboBox()
{
    ui->comboBox->clear();
}

void SubmitTimeWidget::submitTime()
{
    double time = ui->comboBox->currentText().toDouble();

    QString name = ui->nameCombo->currentText().trimmed();
    QString tg = ui->tgEdit->text().trimmed();
    QString email = ui->emailEdit->text().trimmed();

    if(name == "") {
        ui->infoLabel->setText("Fill at least name!");
        QTimer::singleShot(2000, this, [=](){ ui->infoLabel->setText(""); });
        return;
    }

    if(time <= 0.0) {
        ui->infoLabel->setText("Time in wrong format or 0!");
        QTimer::singleShot(2000, this, [=](){ ui->infoLabel->setText(""); });
        return;
    }

    bool success = m_dbm->addTime(name, email, tg, time);

    // Inform about the submit
    if(success) {
        ui->infoLabel->setText("Submitted!");
        QTimer::singleShot(2000, this, [=](){ ui->infoLabel->setText(""); });

        // Clear the edits
        updateNameCombo();
        ui->tgEdit->setText("");
        ui->emailEdit->setText("");
    }
}

void SubmitTimeWidget::updateNameCombo()
{
    // Get all names
    std::vector<QString> names = m_dbm->getPlayerNames();

    // Clear names from combo box
    ui->nameCombo->clear();

    // Add names to combo box
    for(auto name = names.begin(); name != names.end(); ++name) {
        ui->nameCombo->addItem(*name);
    }

    // Set to be empty
    ui->nameCombo->setCurrentText("");
}
