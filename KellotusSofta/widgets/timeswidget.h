#ifndef TIMESWIDGET_H
#define TIMESWIDGET_H

#include <QWidget>

#include "databasemanager.h"

namespace Ui {
class TimesWidget;
}

class TimesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TimesWidget(QWidget *parent = nullptr);
    ~TimesWidget();

    void addBDM(DatabaseManager *dbm);

    void updateData();

private:
    Ui::TimesWidget *ui;

    DatabaseManager *m_dbm;
};

#endif // TIMESWIDGET_H
