#include "../widgets/toptenwidget.h"
#include "ui_toptenwidget.h"

#include <QObjectList>

#include <QDebug> // DEBUGGING

TopTenWidget::TopTenWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TopTenWidget),
    m_dbm(nullptr),
    m_times({})
{
    ui->setupUi(this);
}

TopTenWidget::~TopTenWidget()
{
    delete ui;
}

void TopTenWidget::addDBM(DatabaseManager *dbm)
{
    m_dbm = dbm;
}

void TopTenWidget::updateData()
{
    // Get the top ten times
    std::vector<std::pair<QString, double>> times = m_dbm->getTopTenTimes();

    int n = 1;

    clearTimes();

    // Go through the times
    for(auto time = times.begin(); time != times.end(); ++time) {

        // Create the new widget for time
        ClockedTimeWidget *timeRow = new ClockedTimeWidget(this);
        m_times.emplace_back(timeRow); // Save it to vector

        // Add data to the time widget
        timeRow->addStats(std::to_string(n), time->first.toStdString(), time->second);

        // Add the time widget to the ui layout
        ui->verticalLayout->addWidget(timeRow);

        n++;
    }
}

void TopTenWidget::clearTimes()
{
    // Delete the widgets from the layout
    for(auto time = m_times.begin(); time != m_times.end(); ++time) {
        ui->verticalLayout->removeWidget(*time);
        delete *time;
    }

    // Empty the vector
    m_times.clear();
}
