#include "../widgets/clockedtimewidget.h"
#include "ui_clockedtimewidget.h"

#include <QString>

ClockedTimeWidget::ClockedTimeWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClockedTimeWidget)
{
    ui->setupUi(this);
}

ClockedTimeWidget::~ClockedTimeWidget()
{
    delete ui;
}

void ClockedTimeWidget::addStats(std::string nro, std::string name, double time)
{
    ui->nro->setText(QString::fromStdString(nro));
    ui->name->setText(QString::fromStdString(name));
    ui->time->setText(QString::number(time, 'f', 2));
}
