#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <QAbstractItemModel>

class Stopwatch
{
public:
    using Clock = std::chrono::high_resolution_clock;

    Stopwatch() {}
    ~Stopwatch() {}

    void start();
    void stop();
    void reset();
    double elapsed();
    bool isRunnung() const;
private:
    std::chrono::time_point<Clock> starttime_;
    Clock::duration elapsed_ = Clock::duration::zero();
    bool running_ = false;
};

#endif // STOPWATCH_H
